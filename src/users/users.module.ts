import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserService } from 'src/users/users.service';
import { CreateUserService } from '../create-user/create-user.service';
import { HashService } from '../hashService/hash.service';
import { User, UserModel } from './user.entity';
import { UsersController } from './users.controller';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserModel }]),
  ],
  controllers: [UsersController],
  providers: [UserService, HashService, CreateUserService],
  exports: [UserService],
})
export class UsersModule {}
