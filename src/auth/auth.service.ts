import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { HashService } from '../hashService/hash.service';
import { User } from '../users/user.entity';
import { Role } from './roles/role.enum';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
    private hashService: HashService,
    private jwtService: JwtService,
  ) {}

  async signIn(email: string, pass: string): Promise<any> {
    const user = await this.getUser(email);

    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const iscorrect = await this.hashService.compareHash(pass, user.password);

    if (!iscorrect) {
      throw new UnauthorizedException('Invalid credentials');
    }
    const payload: JWTPayload = { id: user._id, roles: user.roles };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }
  async getUser(email: string): Promise<User | undefined> {
    return this.userModel.findOne({ email: email });
  }
}
export type JWTPayload = {
  id: string;
  roles: Role[];
};
