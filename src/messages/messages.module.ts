import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ChatModelModule } from 'src/chat-model/chat-model.module';
import { Message, MessageModel } from './message.entity';
import { MessagesController } from './messages.controller';
import { MessagesService } from './messages.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Message.name, schema: MessageModel }]),
    ChatModelModule,
  ],
  controllers: [MessagesController],
  providers: [MessagesService],
})
export class MessageModule {}
