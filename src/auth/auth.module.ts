import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import * as dotenv from 'dotenv';
import { HashService } from '../hashService/hash.service';
import { User, UserModel } from '../users/user.entity';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
dotenv.config();
@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserModel }]),
    JwtModule.register({
      global: true,

      secret: process.env.SECRET_JWT_TOKEN,
      signOptions: { expiresIn: '60m' },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, HashService],
})
export class AuthModule {}
