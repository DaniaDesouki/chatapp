import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/auth/auth.guard';
//import { JWTPayload } from 'src/auth/auth.service';
import { Role } from 'src/auth/roles/role.enum';
import { RolesGuard } from 'src/auth/roles/role.guard';
import { Roles } from 'src/auth/roles/roles.decorator';
import { MessageDto } from 'src/users/dto/message.dto';
import { User } from 'src/users/user.decorator';
import { User as userEntity } from 'src/users/user.entity';
import { Message } from './message.entity';
import { MessagesService } from './messages.service';

@UseGuards(AuthGuard)
@Controller('messages')
export class MessagesController {
  constructor(private readonly messagesService: MessagesService) {}
  @Post()
  async sendMessage(
    @Body() messageDto: MessageDto,
    @User() currentUser: userEntity,
  ): Promise<Message> {
    const resp = await this.messagesService.sendMessage(
      messageDto,
      currentUser,
    );

    return resp;
  }
  @UseGuards(AuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get()
  async getAllMessagess(
    @Query('page') page: number = 1,
    @Query('perpage') perPage: number = 5,
  ): Promise<Message[]> {
    return this.messagesService.getAllMessages(page, perPage);
  }
}
