import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ChatModelService } from 'src/chat-model/chat-model.service';
import { MessageDto } from 'src/users/dto/message.dto';
import { User } from 'src/users/user.entity';
import { Message } from './message.entity';
@Injectable()
export class MessagesService {
  constructor(
    @InjectModel(Message.name) private readonly messageModel: Model<Message>,
    private readonly chatModelService: ChatModelService,
  ) {}

  async sendMessage(
    messageDto: MessageDto,
    currentUser: User,
  ): Promise<Message> {
    const message: Message = await this.messageModel.create({
      sender: currentUser.id,
      content: messageDto.content,
    });

    const response = await this.chatModelService.generateResponse(
      message.content,
    );

    const savedResponse: Message = await this.messageModel.create({
      content: response[0].generated_text,
    });
    return savedResponse;
  }

  async getAllMessages(page: number, perPage: number): Promise<Message[]> {
    return this.messageModel
      .find()
      .skip((page - 1) * perPage)
      .limit(perPage)
      .exec();
  }
}
