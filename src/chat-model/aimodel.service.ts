import { HttpService } from '@nestjs/axios/dist';
import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import { firstValueFrom, map } from 'rxjs';

dotenv.config();
@Injectable()
export class AIMessageService {
  constructor(private readonly httpService: HttpService) {}

  private readonly API_TOKEN = process.env.API_TOKEN;

  async generateResponse(prompt: string) {
    try {
      const headers = {
        Authorization: `Bearer ${this.API_TOKEN}`,
      };

      const result = await firstValueFrom(
        this.httpService
          .post<{ generated_text: string }[]>(
            'https://api-inference.huggingface.co/models/gpt2',
            {
              inputs: prompt,
            },
            { headers },
          )
          .pipe(map((resp) => resp.data)),
      );

      return result;
    } catch (error) {
      console.error('Error:', error);
      throw error;
    }
  }
}
