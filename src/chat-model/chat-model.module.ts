import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { AIMessageService } from './aimodel.service';
import { ChatModelService } from './chat-model.service';

@Module({
  imports: [HttpModule],
  providers: [ChatModelService, AIMessageService],
  exports: [ChatModelService],
})
export class ChatModelModule {}
