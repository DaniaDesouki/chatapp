import { Injectable } from '@nestjs/common';
import { AIMessageService } from './aimodel.service';

@Injectable()
export class ChatModelService {
  constructor(private readonly aiModelService: AIMessageService) {}

  async generateResponse(prompt: string) {
    return this.aiModelService.generateResponse(prompt);
  }
}
