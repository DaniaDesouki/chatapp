import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class HashService {
  async hashing(password: string) {
    const hashedPassword = await bcrypt.hash(password, 10);
    return hashedPassword;
  }
  async compareHash(currentpass: string, hashedpass: string): Promise<boolean> {
    return bcrypt.compare(currentpass, hashedpass);
  }
}
