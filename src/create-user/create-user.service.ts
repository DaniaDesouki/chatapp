import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Role } from 'src/auth/roles/role.enum';
import { User } from '../users/user.entity';

@Injectable()
export class CreateUserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
  ) {}

  async createUser(
    useremail: string,
    userpassword: string,
    userrole: Role,
  ): Promise<User> {
    const user = await this.userModel.create({
      email: useremail,
      password: userpassword,
      roles: [userrole],
    });
    return user;
  }
}
