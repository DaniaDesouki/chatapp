import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { Role } from 'src/auth/roles/role.enum';
export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  password: string;

  @IsEnum(Role, { each: true })
  @IsNotEmpty()
  roles: Role;
}
