import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Role } from 'src/auth/roles/role.enum';
import { CreateUserService } from '../create-user/create-user.service';
import { HashService } from '../hashService/hash.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
    private readonly hashService: HashService,
    private readonly createUserService: CreateUserService,
  ) {}

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    const hashedPassword = await this.hashService.hashing(
      createUserDto.password,
    );

    const user = await this.createUserService.createUser(
      createUserDto.email,
      hashedPassword,
      createUserDto.roles,
    );

    await user.save();
    user.password = ' ';
    return user;
  }

  async signUp(createUserDto: CreateUserDto): Promise<User> {
    const hashedPassword = await this.hashService.hashing(
      createUserDto.password,
    );

    const user = await this.createUserService.createUser(
      createUserDto.email,
      hashedPassword,
      Role.User,
    );

    await user.save();
    user.password = ' ';
    return user;
  }

  async getAllUsers(): Promise<User[]> {
    return this.userModel.find().exec();
  }
}
