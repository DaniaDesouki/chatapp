import { ExecutionContext, createParamDecorator } from '@nestjs/common';
export const User = createParamDecorator(
  (data: unknown, executionContext: ExecutionContext) => {
    const httpContext = executionContext.switchToHttp();
    const request = httpContext.getRequest();

    return request.user;
  },
);
